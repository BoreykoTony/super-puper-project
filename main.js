let visible = false

function hasClassName(inElement, inClassName) {
    let regExp = new RegExp('(?:^|\\s+)' + inClassName + '(?:\\s+|$)');
    return regExp.test(inElement.className);
}

function addClassName(inElement, inClassName) {
    if (!hasClassName(inElement, inClassName))
        inElement.className = [inElement.className, inClassName].join(' ');
}

function removeClassName(inElement, inClassName) {
    if (hasClassName(inElement, inClassName)) {
        let regExp = new RegExp('(?:^|\\s+)' + inClassName + '(?:\\s+|$)', 'g');
        let curClasses = inElement.className;
        inElement.className = curClasses.replace(regExp, ' ');
    }
}

function toggleShape() {
    let shape = document.getElementById('shape');
    if (hasClassName(shape, 'ring')) {
        removeClassName(shape, 'ring');
        addClassName(shape, 'cube');
    } else {
        removeClassName(shape, 'cube');
        addClassName(shape, 'ring');
    }
    let stage = document.getElementById('stage');
    if (hasClassName(shape, 'ring')) {
        stage.style.transform = 'translateZ(-200px)';
    } else {
        stage.style.transform = '';
    }
}

function toggleBackVisible() {
    visible = !visible;
    let shape = document.getElementById('shape');
    if (visible) {
        addClassName(shape, 'back');
    } else {
        removeClassName(shape, 'back');
    }
}